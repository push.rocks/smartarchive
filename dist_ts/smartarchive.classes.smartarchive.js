import * as plugins from './smartarchive.plugins.js';
import * as paths from './smartarchive.paths.js';
export class SmartArchive {
    constructor() { }
    /**
     * extracts an archive from a given url
     */
    async extractArchiveFromUrlToFs(urlArg, targetDir) {
        const parsedPath = plugins.path.parse(urlArg);
        const uniqueFileName = plugins.smartunique.uni() + parsedPath.ext;
        plugins.smartfile.fs.ensureDir(paths.nogitDir); // TODO: totally remove caching needs
        const downloadPath = plugins.path.join(paths.nogitDir, uniqueFileName);
        const downloadedArchive = (await plugins.smartrequest.getBinary(urlArg)).body;
        await plugins.smartfile.memory.toFs(downloadedArchive, downloadPath);
        await this.extractArchiveFromFilePathToFs(downloadPath, targetDir);
        await plugins.smartfile.fs.remove(downloadPath);
    }
    /**
     * extracts an archive from a given filePath on disk
     * @param filePathArg
     * @param targetDir
     */
    async extractArchiveFromFilePathToFs(filePathArg, targetDir) {
        console.log(`extracting ${filePathArg}`);
    }
    /**
     * extracts to Observable
     */
    async extractArchiveFromBufferToObservable(bufferArg) {
        const intake = new plugins.smartstream.StreamIntake();
        const replaySubject = new plugins.smartrx.rxjs.ReplaySubject();
        const readableStream = intake.getReadableStream();
        const extractPipeStop = plugins.tarStream.extract();
        extractPipeStop.on('entry', (header, stream, next) => {
            let fileBuffer;
            stream.on('data', (chunkArg) => {
                if (!fileBuffer) {
                    fileBuffer = chunkArg;
                }
                else {
                    fileBuffer = Buffer.concat([fileBuffer, chunkArg]);
                }
            });
            stream.on('end', () => {
                replaySubject.next(new plugins.smartfile.Smartfile({
                    base: null,
                    contentBuffer: fileBuffer,
                    path: `${header.name}`
                }));
                next();
            });
            stream.resume();
        });
        extractPipeStop.on('finish', () => {
            replaySubject.complete();
        });
        // lets run the stream
        readableStream
            .pipe(plugins.gunzipMaybe())
            .pipe(extractPipeStop);
        intake.pushData(bufferArg);
        intake.signalEnd();
        return replaySubject;
    }
    /**
     * extracts to Observable
     */
    async extractArchiveFromUrlToObservable(urlArg) {
        const response = await plugins.smartrequest.getBinary(urlArg);
        const replaySubject = this.extractArchiveFromBufferToObservable(response.body);
        return replaySubject;
    }
    // TODO
    async extractArchiveFromUrlToStream() {
    }
    // TODO
    async extractArchiveFromFilePathToStream() { }
    // TODO
    async extractArchiveFromStreamToStream() { }
    // TODO
    async packFromStreamToStream() { }
    // TODO
    async packFromDirPathToStream() { }
    // TODO
    async packFromDirPathToFs() { }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic21hcnRhcmNoaXZlLmNsYXNzZXMuc21hcnRhcmNoaXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vdHMvc21hcnRhcmNoaXZlLmNsYXNzZXMuc21hcnRhcmNoaXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sS0FBSyxPQUFPLE1BQU0sMkJBQTJCLENBQUM7QUFDckQsT0FBTyxLQUFLLEtBQUssTUFBTSx5QkFBeUIsQ0FBQztBQUVqRCxNQUFNLE9BQU8sWUFBWTtJQUV2QixnQkFBZSxDQUFDO0lBRWhCOztPQUVHO0lBQ0ksS0FBSyxDQUFDLHlCQUF5QixDQUFDLE1BQWMsRUFBRSxTQUFpQjtRQUN0RSxNQUFNLFVBQVUsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QyxNQUFNLGNBQWMsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLFVBQVUsQ0FBQyxHQUFHLENBQUM7UUFDbEUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLHFDQUFxQztRQUNyRixNQUFNLFlBQVksR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLGNBQWMsQ0FBQyxDQUFDO1FBQ3ZFLE1BQU0saUJBQWlCLEdBQUcsQ0FBQyxNQUFNLE9BQU8sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQzlFLE1BQU0sT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLFlBQVksQ0FBQyxDQUFDO1FBQ3JFLE1BQU0sSUFBSSxDQUFDLDhCQUE4QixDQUFDLFlBQVksRUFBRSxTQUFTLENBQUMsQ0FBQztRQUNuRSxNQUFNLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxXQUFtQixFQUFFLFNBQWlCO1FBQ2hGLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxXQUFXLEVBQUUsQ0FBQyxDQUFDO0lBRTNDLENBQUM7SUFFRDs7T0FFRztJQUNJLEtBQUssQ0FBQyxvQ0FBb0MsQ0FDL0MsU0FBaUI7UUFFakIsTUFBTSxNQUFNLEdBQUcsSUFBSSxPQUFPLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3RELE1BQU0sYUFBYSxHQUFHLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUErQixDQUFDO1FBQzVGLE1BQU0sY0FBYyxHQUFHLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQ2xELE1BQU0sZUFBZSxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDcEQsZUFBZSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxFQUFFO1lBQ25ELElBQUksVUFBa0IsQ0FBQztZQUN2QixNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFO2dCQUM3QixJQUFJLENBQUMsVUFBVSxFQUFFO29CQUNmLFVBQVUsR0FBRyxRQUFRLENBQUM7aUJBQ3ZCO3FCQUFNO29CQUNMLFVBQVUsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUM7aUJBQ3BEO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxNQUFNLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBRSxHQUFHLEVBQUU7Z0JBQ3BCLGFBQWEsQ0FBQyxJQUFJLENBQ2hCLElBQUksT0FBTyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7b0JBQzlCLElBQUksRUFBRSxJQUFJO29CQUNWLGFBQWEsRUFBRSxVQUFVO29CQUN6QixJQUFJLEVBQUUsR0FBRyxNQUFNLENBQUMsSUFBSSxFQUFFO2lCQUN2QixDQUFDLENBQ0gsQ0FBQztnQkFDRixJQUFJLEVBQUUsQ0FBQztZQUNULENBQUMsQ0FBQyxDQUFDO1lBQ0gsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsZUFBZSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFO1lBQ2hDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUMzQixDQUFDLENBQUMsQ0FBQztRQUNILHNCQUFzQjtRQUN0QixjQUFjO2FBQ1gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUMzQixJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDekIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMzQixNQUFNLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDbkIsT0FBTyxhQUFhLENBQUM7SUFDdkIsQ0FBQztJQUVEOztPQUVHO0lBQ0ssS0FBSyxDQUFDLGlDQUFpQyxDQUM3QyxNQUFjO1FBRWQsTUFBTSxRQUFRLEdBQUcsTUFBTSxPQUFPLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5RCxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsb0NBQW9DLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9FLE9BQU8sYUFBYSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxPQUFPO0lBQ0EsS0FBSyxDQUFDLDZCQUE2QjtJQUUxQyxDQUFDO0lBRUQsT0FBTztJQUNBLEtBQUssQ0FBQyxrQ0FBa0MsS0FBSSxDQUFDO0lBRXBELE9BQU87SUFDQSxLQUFLLENBQUMsZ0NBQWdDLEtBQUksQ0FBQztJQUVsRCxPQUFPO0lBQ0EsS0FBSyxDQUFDLHNCQUFzQixLQUFJLENBQUM7SUFFeEMsT0FBTztJQUNBLEtBQUssQ0FBQyx1QkFBdUIsS0FBSSxDQUFDO0lBRXpDLE9BQU87SUFDQSxLQUFLLENBQUMsbUJBQW1CLEtBQUksQ0FBQztDQUN0QyJ9