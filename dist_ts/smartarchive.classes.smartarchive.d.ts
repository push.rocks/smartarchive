/// <reference types="node" resolution-mode="require"/>
import * as plugins from './smartarchive.plugins.js';
export declare class SmartArchive {
    archiveDirectory: string;
    constructor();
    /**
     * extracts an archive from a given url
     */
    extractArchiveFromUrlToFs(urlArg: string, targetDir: string): Promise<void>;
    /**
     * extracts an archive from a given filePath on disk
     * @param filePathArg
     * @param targetDir
     */
    extractArchiveFromFilePathToFs(filePathArg: string, targetDir: string): Promise<void>;
    /**
     * extracts to Observable
     */
    extractArchiveFromBufferToObservable(bufferArg: Buffer): Promise<plugins.smartrx.rxjs.ReplaySubject<plugins.smartfile.Smartfile>>;
    /**
     * extracts to Observable
     */
    extractArchiveFromUrlToObservable(urlArg: string): Promise<plugins.smartrx.rxjs.ReplaySubject<plugins.smartfile.Smartfile>>;
    extractArchiveFromUrlToStream(): Promise<void>;
    extractArchiveFromFilePathToStream(): Promise<void>;
    extractArchiveFromStreamToStream(): Promise<void>;
    packFromStreamToStream(): Promise<void>;
    packFromDirPathToStream(): Promise<void>;
    packFromDirPathToFs(): Promise<void>;
}
