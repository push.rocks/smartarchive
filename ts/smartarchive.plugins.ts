// node native scope
import * as path from 'path';

export { path };

// @pushrocks scope
import * as smartfile from '@pushrocks/smartfile';
import * as smartpath from '@pushrocks/smartpath';
import * as smartpromise from '@pushrocks/smartpromise';
import * as smartrequest from '@pushrocks/smartrequest';
import * as smartunique from '@pushrocks/smartunique';
import * as smartstream from '@pushrocks/smartstream';
import * as smartrx from '@pushrocks/smartrx';

export { smartfile, smartpath, smartpromise, smartrequest, smartunique, smartstream, smartrx };

// third party scope
import gunzipMaybe from 'gunzip-maybe';

// @ts-ignore
import tar from 'tar';
import tarStream from 'tar-stream';

export { gunzipMaybe, tar, tarStream };
