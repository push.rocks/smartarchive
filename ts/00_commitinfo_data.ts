/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartarchive',
  version: '3.0.6',
  description: 'work with archives'
}
